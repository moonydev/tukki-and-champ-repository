﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fire : MonoBehaviour {

    //references
    private BoxCollider2D col;
    GameObject tongueVis;
    SpriteRenderer tongueRendr;
    Animator tongueAnim;
    Interface interf;
    public Sprite empty;

    //status info
    public enum tongueState { idle, extending, climax, retracting };
    [HideInInspector]
    public tongueState currentTongueState;
    float timeSinceExtension;

    //status info handled by other scripts
    [HideInInspector]
    public bool cursorOverButton;

	void Start () {
        //reference assignment
        col = gameObject.GetComponent<BoxCollider2D>();
        tongueVis = GameObject.Find("Tongue Visuals");
        tongueRendr = tongueVis.GetComponent<SpriteRenderer>();
        tongueAnim = tongueVis.GetComponent<Animator>();
        interf = FindObjectOfType<Interface>().GetComponent<Interface>();

        //set default tongue state
        currentTongueState = tongueState.idle;
        col.enabled = false;
	}

    public IEnumerator Shoot(float tongueSpeed, float tongueRange, float climaxDelay)
    {
        if (currentTongueState == tongueState.idle)
        {
            currentTongueState = tongueState.extending;
            //tongueAnim.ResetTrigger("stop");
            tongueAnim.SetTrigger("start");
            col.enabled = true;
            tongueRendr.enabled = true;
            yield return null;
        }
        while (currentTongueState == tongueState.extending)
        {
            if (interf.currentGameState == 0)
            {
                if (transform.localPosition.y < tongueRange)
                    transform.localPosition += new Vector3(0f, tongueSpeed * Time.deltaTime, 0f);
                else
                {
                    currentTongueState = tongueState.climax;
                    timeSinceExtension = 0f;
                }
            }
            yield return null;
        }
        while (currentTongueState == tongueState.climax)
        {
            if (interf.currentGameState == 0)
            {
                if (timeSinceExtension < climaxDelay)
                    timeSinceExtension += Time.deltaTime;
                else
                    currentTongueState = tongueState.retracting;
            }
            yield return null;
        }
        while (currentTongueState == tongueState.retracting)
        {
            if (interf.currentGameState == 0)
            {
                if (transform.localPosition.y > 0f)
                    transform.localPosition -= new Vector3(0f, tongueSpeed * Time.deltaTime, 0f);
                else
                    Idle();
            }
            yield return null;                
        }
    }


    public void Idle()
    {
        currentTongueState = tongueState.idle;
        transform.localPosition = Vector3.zero;
        col.enabled = false;
        //tongueRendr.enabled = false;
        //tongueRendr.sprite = null;
        tongueAnim.ResetTrigger("start");
        //tongueAnim.SetTrigger("stop");
    }

}
