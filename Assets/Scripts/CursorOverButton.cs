﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CursorOverButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{

    Interface interf;

    private void Start()
    {
        interf = FindObjectOfType<Interface>().GetComponent<Interface>();
    }

    //Do this when the cursor enters the rect area of this selectable UI object.
    public void OnPointerEnter(PointerEventData eventData)
    {
        interf.cursorOnButton = true;
    }

    //Do this when the cursor exits the rect area of this selectable UI object.
    public void OnPointerExit(PointerEventData eventData)
    {
        interf.cursorOnButton = false;
    }
}
