﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadSwitch : MonoBehaviour {

    //parameters
    public bool rotateHead;
    [Range(0f,1f)]
    public float upRotResponsiveness;
    [Range(0f, 1f)]
    public float upHorRotResponsiveness;
    [Range(0f, 1f)]
    public float horRotResponsiveness;
    [Range(0f, 1f)]
    public float downHorRotResponsiveness;
    [Range(0f, 1f)]
    public float downRotResponsiveness;
    public float degLimitUp;
    public float degLimitUpLeft;
    public float degLimitLeft;
    public float degLimitDownLeft;
    float degLimitDown;
    float degLimitDownRight;
    float degLimitRight;
    float degLimitUpRight;

    //references
    SpriteRenderer rendr;
    GameObject aim;
    Fire fire;
    public Sprite lookUp;
    public Sprite lookRightUp;
    public Sprite lookRight;
    public Sprite lookRightDown;
    public Sprite lookDown;    
    public Sprite shootUp;
    public Sprite shootDown;
    public Sprite shootHor;

    //status info
    float aimAngle;
    bool sideview;
    float  headAngle;
    bool isFlipped;

	void Start () {
        //reference assignments
        rendr = GetComponent<SpriteRenderer>();
        aim = GameObject.FindGameObjectWithTag("Aim");
        fire = GameObject.FindGameObjectWithTag("Tongue").GetComponent<Fire>();

        degLimitDown = 360f - degLimitDownLeft;
        degLimitDownRight = 360f - degLimitLeft;
        degLimitRight = 360f - degLimitUpLeft;
        degLimitUpRight = 360f - degLimitUp;
	}

    private void OnEnable()
    {
        CharacterManager.FlipEvent += OnFlip;
    }

    private void OnDisable()
    {
        CharacterManager.FlipEvent -= OnFlip;
    }

    void Update () {

        // HEAD SWITCHER
        //determine where champ is aiming
        aimAngle = aim.transform.eulerAngles.z;
        //determine wether player is shooting
        bool isFiring;
        if (fire.currentTongueState == 0)
            isFiring = false;
        else
            isFiring = true;

        //for each aim direction:
        //step 1: choose the corresponding head sprite for champ (make him face in the general direction and open his mouth if shooting)
        //step 2: calculate the angle to turn the head to make it face the direction of aim more accurately, while avoiding unnatural poses
        //step 3: save for later wether the head is in sideview
        //if aiming up
        if (aimAngle < degLimitUp || aimAngle > 360f - degLimitUp)
        {
            //step 1
            if (isFiring)
                rendr.sprite = shootUp;
            else
                rendr.sprite = lookUp;
            //steps 2 + 3
            //headAngle = aimAngle;
            if (aimAngle < degLimitUp)
                headAngle = upRotResponsiveness * aimAngle;
            else
                headAngle = upRotResponsiveness * (aimAngle - 360f);
            sideview = false;
        }
        //if aiming left+up
        else if (aimAngle < degLimitUpLeft)
        {
            //steps 1 + 2
            if (isFiring)
                rendr.sprite = shootHor;
            else
                rendr.sprite = lookRightUp;
            rendr.flipX = true;
            //step 2
            //headAngle = Mathf.Clamp(aimAngle - 67.5f, -22.5f, 0f);
            headAngle = upHorRotResponsiveness * (aimAngle - degLimitUpLeft + (degLimitUpLeft - degLimitUp) / 2);
            //step 3
            sideview = true;
        }
        //if aiming left
        else if (aimAngle < degLimitLeft) //8ths: 112.5
        {
            //steps 1 + 2
            if (isFiring)
                rendr.sprite = shootHor;
            else
                rendr.sprite = lookRight;
            rendr.flipX = true;
            //step 2
            //headAngle = aimAngle - 80f;
            headAngle = horRotResponsiveness * (aimAngle - degLimitLeft + (degLimitLeft - degLimitUpLeft) / 2);
            //step 3
            sideview = true;
        }
        //if aiming left+down
        else if (aimAngle < degLimitDownLeft) //8ths: 157.5
        {
            //steps 1 + 2
            if (isFiring)
                rendr.sprite = shootHor;
            else
                rendr.sprite = lookRightDown;
            rendr.flipX = true;
            //step 2
            //headAngle = Mathf.Clamp(aimAngle - 112.5f, 0f, 11.25f);
            headAngle = downHorRotResponsiveness * (aimAngle - degLimitDownLeft + (degLimitDownLeft - degLimitLeft) / 2);
            //step 3
            sideview = true;
        }
        //if aiming down
        else if (aimAngle < degLimitDown) //8ths: 202.5f
        {
            //step 1
            if (isFiring)
                rendr.sprite = shootDown;
            else
                rendr.sprite = lookDown;
            //steps 2 + 3
            //headAngle = aimAngle - 180f;
            headAngle = downRotResponsiveness * (aimAngle - degLimitDown + (degLimitDown - degLimitDownLeft) / 2);
            sideview = false;
        }
        //if aiming right+down
        else if (aimAngle < degLimitDownRight) //8ths: 247.5
        {
            if (isFiring)
                rendr.sprite = shootHor;
            else
                rendr.sprite = lookRightDown;
            rendr.flipX = false;
            //step 2
            //headAngle = Mathf.Clamp(aimAngle - 247.5f, -11.25f, 0f); //pre: 247.5
            headAngle = downHorRotResponsiveness * (aimAngle - degLimitDownRight + (degLimitDownRight - degLimitDown) / 2);
            //step 3
            sideview = true;
        }
        //if aiming right
        else if (aimAngle < degLimitRight) // 8ths: 292.5
        {
            if (isFiring)
                rendr.sprite = shootHor;
            else
                rendr.sprite = lookRight;
            rendr.flipX = false;
            //step 2
            //headAngle = aimAngle - 270f;
            headAngle = horRotResponsiveness * (aimAngle - degLimitRight + (degLimitRight - degLimitDownRight) / 2);
            //step 3
            sideview = true;
        }
        //if aiming right+up
        else if (aimAngle < degLimitUpRight)
        {
            if (isFiring)
                rendr.sprite = shootHor;
            else
                rendr.sprite = lookRightUp;
            rendr.flipX = false;
            //step 2
            //headAngle = Mathf.Clamp(aimAngle - 292.5f, 0f, 22.5f);
            headAngle = upHorRotResponsiveness * (aimAngle - degLimitUpRight + (degLimitUpRight - degLimitRight) / 2);
            //step 3
            sideview = true;
        }
        else
            Debug.LogError("no head sprite could be chosen, check degree borders in headswitch script");


        //if desired, rotate head to face exact direction of aim
        if (rotateHead)
            transform.eulerAngles = new Vector3(0f, 0f, headAngle);
	}

    public void OnFlip(bool desired)
    {
        //mirror sprite unless head is in sideview or orientation is already correct
        if (!sideview && isFlipped != desired)
        {
            rendr.flipX = !rendr.flipX;
            isFlipped = !isFlipped;
        }
    }
}
