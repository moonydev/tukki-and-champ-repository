﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterManager : MonoBehaviour {

    //references
    Interface interf;
    public GameObject tukkiBody;
    Rigidbody2D tukkiRbody;
    Flight tukkiFlight;
    SpriteRenderer tukkiRendr;
    public GameObject champRotator;
    public GameObject champTongueVis;
    Aim champTargeting;
    Fire champFire;
    Animator champTongueAnim;

    Transform champHeadTrans;
    HeadSwitch champHeadSwitch;

    //event systems
    public delegate void FlipAction(bool flip);
    public static event FlipAction FlipEvent;
    public delegate void FreezeAction(bool freeze);
    public static event FreezeAction FreezeEvent;

    //parameters
    public Vector2 startingPos;
    public float tukkiMoveSpeed;
    public float tukkiFlapForce;
    public float tukkiFlapCooldown;
    public float champTongueSpeed;
    public float champTongueRange;
    public bool champRestrictAimIdle;
    public float champAimIdleSpeedLimitDegPerSec;
    public bool champRestrictAimShooting;
    public float champAimShootingSpeedLimitDegPerSec;

    public bool champAnimShowcase;
    public float champAnimShowcaseSpeed;

    //var iables resulting from parameters
    float champTongueClimaxDelay; //amount of time champ's tongue collider stays fully extended

    //status info
    bool charsFlipped;
    float tukkiTimeSinceLastFlap;
    float tukkiCurrentMoveSpeed;

    float currentChampAnimShowcaseSpeed;

    private void Start()
    {
        //reference assignment
        interf = GetComponent<Interface>();
        tukkiFlight = tukkiBody.GetComponent<Flight>();
        tukkiRbody = tukkiBody.GetComponent<Rigidbody2D>();
        tukkiRendr = tukkiBody.GetComponent<SpriteRenderer>();
        champTargeting = champRotator.GetComponent<Aim>();
        champFire = GameObject.Find("Tongue Hitreg").GetComponent<Fire>();
        champTongueAnim = champTongueVis.GetComponent<Animator>();

        champHeadTrans = FindObjectOfType<HeadSwitch>().GetComponent<Transform>();
        champHeadSwitch = FindObjectOfType<HeadSwitch>().GetComponent<HeadSwitch>();

        //prepare showcase
        currentChampAnimShowcaseSpeed = champAnimShowcaseSpeed;

        //ensure animation and hitregistration of champ's tongue match up
        TongueAnimationHitregSync();

        //prepare characters to begin
        Default();
    }

    private void Update()
    {
        if (interf.currentGameState == 0)
        {
            //TUKKI INPUT
            //turn around
            if (Input.GetKeyDown(KeyCode.LeftControl))
            {
                charsFlipped = !charsFlipped;
                OnFlip(charsFlipped);
                tukkiCurrentMoveSpeed = -tukkiCurrentMoveSpeed;
                tukkiFlight.TurnAround(tukkiCurrentMoveSpeed);
            }
            //flap wings
            if (Input.GetKeyDown(KeyCode.Space) && tukkiTimeSinceLastFlap >= tukkiFlapCooldown)
            {
                tukkiFlight.Flap(tukkiCurrentMoveSpeed, tukkiFlapForce);
                tukkiTimeSinceLastFlap = 0f;
            }
            else
                tukkiTimeSinceLastFlap += Time.deltaTime;

            //CHAMP INPUT
            //showcase turning animatons
            if (Input.GetKeyDown(KeyCode.Mouse1))
                champAnimShowcase = !champAnimShowcase;
            if (Input.GetKeyDown(KeyCode.Mouse2))
            {
                champHeadSwitch.rotateHead = !champHeadSwitch.rotateHead;
                champHeadTrans.eulerAngles = Vector3.zero;
            }
            else if (champAnimShowcase)
            {
                if (Input.GetKeyDown(KeyCode.Mouse4))
                    currentChampAnimShowcaseSpeed = -champAnimShowcaseSpeed;
                if (Input.GetKeyDown(KeyCode.Mouse3))
                    currentChampAnimShowcaseSpeed = champAnimShowcaseSpeed;
                champRotator.transform.eulerAngles += new Vector3(0f, 0f, currentChampAnimShowcaseSpeed * Time.deltaTime);
                if (champRotator.transform.eulerAngles.z < 0f)
                    champRotator.transform.eulerAngles += new Vector3(0f, 0f, 360f);
                else if (champRotator.transform.eulerAngles.z > 360f)
                    champRotator.transform.eulerAngles -= new Vector3(0f, 0f, 360f);
            }
            //aim
            else if (champFire.currentTongueState == 0)
                champTargeting.TargetCursor(champRestrictAimIdle, champAimIdleSpeedLimitDegPerSec);
            else
                champTargeting.TargetCursor(champRestrictAimShooting, champAimShootingSpeedLimitDegPerSec);
            //shoot
            if (Input.GetKeyDown(KeyCode.Mouse0) && !interf.cursorOnButton)
                StartCoroutine(champFire.Shoot(champTongueSpeed, champTongueRange, champTongueClimaxDelay));
        }
    }

    public void Default()
    {
        tukkiBody.transform.position = startingPos;
        OnFlip(false);
        charsFlipped = false;
        tukkiCurrentMoveSpeed = tukkiMoveSpeed;
        tukkiRbody.velocity = new Vector2(tukkiCurrentMoveSpeed, 0f);
        champRotator.transform.eulerAngles = Vector3.up;
        champFire.Idle();
    }

    //call this when changing champ's tongue parameters at runtime
    public void TongueAnimationHitregSync()
    {
        //sync animation speed of champ's tongue with its hitreg speed
        float shotDuration = champTongueRange / champTongueSpeed * 2f;
        float speedMultiplier = 1 / shotDuration * 1.3f;
        champTongueAnim.SetFloat("firingSpeed", speedMultiplier);

        //sync champ's tongue size with its hitreg range
        float sizeMultiplier = champTongueRange * .38f;
        champTongueVis.transform.localScale = new Vector3(Mathf.Clamp(sizeMultiplier, 1f, 2.5f), sizeMultiplier, 1f);

        //calculate correct ratio between climax delay and speed + range
        champTongueClimaxDelay = 1f / champTongueSpeed * champTongueRange / 2.5f;
    }

    public void OnFlip(bool desired)
    {
        if (FlipEvent != null)
            FlipEvent(desired);
        tukkiRendr.flipX = desired;
    }

    public void OnFreeze(bool desired)
    {
        if (FreezeEvent != null)
            FreezeEvent(desired);
        tukkiRbody.simulated = !desired;
    }
}
