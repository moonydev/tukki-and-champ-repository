﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackPlayers : MonoBehaviour {

    //rederences
    Transform tukkiTr;

    //parameters
    public float trackingSpeed;
    public Vector2 constraints;
    public float startTrackingDist;

    void Start()
    {
        //reference assignment
        tukkiTr = GameObject.FindGameObjectWithTag("Tukki").GetComponent<Transform>();
    }

    void Update()
    {
        //the vector that leads from the camera to tukki
        Vector3 toPlayer = tukkiTr.position - transform.position;
        //keep camera at same distance from playing field
        toPlayer.z = 0f;
        if (toPlayer.magnitude > startTrackingDist)
        {
            //clone camera positon and move it towards player (normalized) at the specified speed
            Vector3 newPos = transform.position + toPlayer * trackingSpeed * Time.deltaTime;
            //make sure it stays within constraints
            newPos.x = Mathf.Clamp(newPos.x, -constraints.x, constraints.x);
            newPos.y = Mathf.Clamp(newPos.y, -constraints.y, constraints.y);
            //apply new position;
            transform.position = newPos;
        }
    }
}
