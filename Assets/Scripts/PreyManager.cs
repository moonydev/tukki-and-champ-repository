﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PreyManager : MonoBehaviour {

    //parameters
    public enum spawnOpts { waves, single };
    public spawnOpts spawningBehaviour;
    public int preyCountLimit;
    public Vector2 spawnArea;
    public float spawnDistance;
    public float minDistToPlayerPos;
    [Range (0, 100)]
    public int mosquitoPercentage;
    public bool artificialScaling;     //don't use with perspective cam 

    //references
    public GameObject staticPreyPrefab;
    public GameObject mosquitoPreyPrefab;
    GameObject tukki;

    //status info   
    int preyCount;
    bool spawningParametersValidated;
    public int failedSpawnAttempts;
    bool firstWave;

    //local variables
    float longerSpawnAreaSide;
    float shorterSpawnAreaSide;
    Vector2 mosqSpawnArea;
    float mosqRangeOffset;

    void Start()
    {
        //reference assignment
        tukki = GameObject.FindGameObjectWithTag("Tukki");
        //check if parameters leave enough space for spawning
        CheckSpawningParameterValidity(spawnArea, false);
        //limit spawn area for moving prey so they don't fly off screen
        if (mosquitoPercentage > 0 && spawningParametersValidated == true)
        {
            mosqRangeOffset = mosquitoPreyPrefab.GetComponent<MosquitoPattern>().range;
            mosqSpawnArea = spawnArea - new Vector2(mosqRangeOffset, mosqRangeOffset);
            CheckSpawningParameterValidity(mosqSpawnArea, true);
        }
        //determine which side of the spawn area rectangle is longer/shorter
        longerSpawnAreaSide = spawnArea.x > spawnArea.y ? spawnArea.x : spawnArea.y;
        shorterSpawnAreaSide = spawnArea.x < spawnArea.y ? spawnArea.x : spawnArea.y;
        //ensure that the minDistToPlayerPos setting still leaves room for spawning even with tukki in center
        minDistToPlayerPos = Mathf.Clamp(minDistToPlayerPos, 0f, longerSpawnAreaSide * .9f);
        //spawn first wave
        firstWave = true;
        if (spawningParametersValidated)
            SpawnWave();
    }

    void SpawnWave()
    {
        for (int i = 0; i < preyCountLimit; i++)        
            Spawn();
        firstWave = false;
    }

    void Spawn()
    {
        //local variables
        GameObject preyClone;
        Vector2 correctSpawnArea;
        //choose and spawn a prefab
        if (Random.Range(0f, 100f) > mosquitoPercentage)
        {
            preyClone = Instantiate(staticPreyPrefab);
            correctSpawnArea = spawnArea;
        }
        else
        {
            preyClone = Instantiate(mosquitoPreyPrefab);
            correctSpawnArea = mosqSpawnArea;
        }
        //choose position so that moving prey will never move out of the spawn area
        float xLim = correctSpawnArea.x;
        float yLim = correctSpawnArea.y;
        Vector3 spawnPos = new Vector3(Random.Range(-xLim, xLim), Random.Range(-yLim, yLim), 0f);
        //avoid spawning prey in a radius minDistToPlayerPos around tukki
        while (Vector3.Distance(spawnPos, tukki.transform.position) < minDistToPlayerPos)
        {
            failedSpawnAttempts++;
            spawnPos = new Vector3(Random.Range(-xLim, xLim), Random.Range(-yLim, yLim), 0f);
        }
        Prey scriptClone = preyClone.GetComponent<Prey>();
        scriptClone.goalPos = spawnPos;
        //if part of first wave
        if (firstWave)
        {
            //just appear
            scriptClone.isFlyingIn = false;
            preyClone.transform.position = spawnPos;
        }
        //otherwise
        else
        {
            //spawn prey outside of visible area (it will then fly to its actual spawning position)
            scriptClone.isFlyingIn = true;
            preyClone.transform.position = spawnPos.normalized * spawnDistance;
        }
        if (!artificialScaling)
            preyClone.transform.position += new Vector3(0f, 0f, -10f);
        preyCount++;
    }

    public void OnCollect()
    {
        preyCount--;
        switch (spawningBehaviour)
        {
            case spawnOpts.waves:
                if (GameObject.FindGameObjectsWithTag("Prey").Length <= 1)
                {
                    preyCount = 0;
                    SpawnWave();
                }
                break;
            case spawnOpts.single:
                while (preyCount < preyCountLimit)
                    Spawn();
                break;
            default:
                Debug.Log("spawn options error");
                break;
        }
    }

    public void Restart()
    {
        GameObject[] preyInScene = GameObject.FindGameObjectsWithTag("Prey");
        foreach (GameObject p in preyInScene)
            Destroy(p);
        preyCount = 0;
        SpawnWave();
    }

    void CheckSpawningParameterValidity(Vector2 spawnAreaToCheck, bool isForMovingPrey)
    {
        if (spawnAreaToCheck.x < minDistToPlayerPos || spawnAreaToCheck.y < minDistToPlayerPos)
        {
            spawningParametersValidated = false;
            if (isForMovingPrey)
                Debug.LogError("Not enough space to spawn moving prey. Set moving prey percentages to 0, lower movement range, increase spawn area size, or lower minDistToPlayerPos value.");
            else
                Debug.LogError("Not enough space to spawn any prey. Increase spawn area size or lower minDistToPlayerPos value.");
        }
        else
            spawningParametersValidated = true;
    }
}
