﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class Flip : MonoBehaviour {

    //references
    SpriteRenderer rendr;

    //status info
    bool isFlipped;

	void Start () {
        //reference assignment
        rendr = GetComponent<SpriteRenderer>();
	}

    private void OnEnable()
    {
        CharacterManager.FlipEvent += FlipIt;
    }

    private void OnDisable()
    {
        CharacterManager.FlipEvent -= FlipIt;
    }

    void FlipIt (bool desired) {
        //only if orientation is not already correct
        if (isFlipped != desired)
        {
            //invert x position
            transform.localPosition += new Vector3(-transform.localPosition.x * 2, 0f, 0f);
            //invert rotation
            transform.localRotation = Quaternion.Inverse(transform.localRotation);
            //flip sprite
            rendr.flipX = !rendr.flipX;
            //save status
            isFlipped = !isFlipped;
        }
	}
}
