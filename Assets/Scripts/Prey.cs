﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Prey : MonoBehaviour {

    //references
    PreyManager preyMan;
    Interface interf;
    BoxCollider2D col;

    //parameters
    public float flyInSpeed;
    public float flyInScaleIncrease;

    //status info
    [HideInInspector]
    public bool isFlyingIn;

    //local variables
    [HideInInspector]
    public Vector3 goalPos;
    Vector3 toGoalPos;
    Vector3 startPos;
    float fullDist;
    Vector3 normalScale;
    float randWaitTime;

    private void Start()
    {
        //reference assignment
        preyMan = FindObjectOfType<PreyManager>().GetComponent<PreyManager>();
        interf = FindObjectOfType<Interface>().GetComponent<Interface>();
        col = GetComponent<BoxCollider2D>();
        //starting state
        if (isFlyingIn)
        {
            col.enabled = false;
            normalScale = transform.localScale;
            startPos = transform.position;
            toGoalPos = goalPos - transform.position;
            fullDist = Vector3.Distance(transform.position, goalPos);
            randWaitTime = Random.Range(0f, 1f);
        }
    }

    private void Update()
    {
        //if the games running and this bug is currently flying in
        if (interf.currentGameState == 0 && isFlyingIn)
        {
            if (randWaitTime > 0f)
                randWaitTime -= Time.deltaTime;
            else
            {
                //move toward goal position
                transform.position += toGoalPos.normalized * flyInSpeed * Time.deltaTime;
                //the closer to goal position, the smaller the bug (illusion of depth)
                if (preyMan.artificialScaling)
                    transform.localScale = normalScale + (normalScale * flyInScaleIncrease * (Vector3.Distance(transform.position, goalPos) / fullDist));
                //if bug moved past goal position
                if (Vector3.Distance(transform.position, startPos) > fullDist)
                {
                    //stop moving and enable collision
                    isFlyingIn = false;
                    col.enabled = true;
                }
            }
        }
    }

    //when collected
    private void OnTriggerEnter2D(Collider2D collision)
    {
        //notify interface for score tracking
        if (collision.gameObject.tag == "Tukki")
            interf.IncreaseScore(true);
        else if (collision.gameObject.tag == "Tongue")
            interf.IncreaseScore(false);
        //notify prey manager for respawning
        preyMan.OnCollect();
        //self destroy
        Destroy(gameObject);
    }
}
