﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flight : MonoBehaviour {

    //references
    private Rigidbody2D rbody;

    //sound stuff
    public AudioSource flaps;
    public AudioClip flap1;
    public AudioClip flap2;
    public AudioClip flap3;

    public int randnum;

    private void Start()
    {
        rbody = GetComponent<Rigidbody2D>();
    }

    public void TurnAround (float horSpeed)
    {
        rbody.velocity = new Vector2(horSpeed, rbody.velocity.y); //change horizontal velocity
    }

    public void Flap (float horSpeed, float pushForce)
    {
        rbody.velocity = new Vector2(horSpeed, 0f); //make sure bird moves horizontally, stop any vertical movement
        rbody.AddForce(new Vector2(0f, pushForce)); //push bird upward

        randnum = Random.Range(1, 4);
        if (randnum == 1)
        {
            flaps.PlayOneShot(flap1, 0.7f);
        }
        else if (randnum == 2)
        {
            flaps.PlayOneShot(flap2, 0.7f);  
        }
        else if (randnum == 3)
        {
            flaps.PlayOneShot(flap3, 0.5f);
        }
    }
}
