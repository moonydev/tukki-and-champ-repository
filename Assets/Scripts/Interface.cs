﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Interface : MonoBehaviour
{
    //references
    CharacterManager charMan;
    PreyManager preyMan;
    
    public Text pauseText;
    public Text countdown;
    public Text scoreText;
    public Text resultText;

    public GameObject restartButton;
    public GameObject pauseButton;
    public GameObject unpauseButton;

    //sound stuff
    public AudioSource GMaudiosource;
    public AudioClip countdownstatic;
    public AudioClip pausesound;
    public bool preventsoundonstartup;

    public AudioSource GMbackground;
    public AudioClip backgroundambience;

    //parameters
    public int scoreLimit;
    public Vector2 startingPos;
    public bool skipCountdown;

    //status info
    public enum gameState { running, paused, over};
    [HideInInspector]
    public gameState currentGameState;
    [HideInInspector]
    public bool cursorOnButton;
    int tukkiScore;
    int champScore;
    float countdownTimeRemaining;

    private void Start()
    {
        //reference assignment
        charMan = GetComponent<CharacterManager>();
        preyMan = GetComponent<PreyManager>();

        //start game
        Pause();
        InitiateUnpause();
    }

    void Update()
    {
        //toggle pause with esc
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (currentGameState == gameState.running)           
                Pause();
            else if (currentGameState == gameState.paused)
                InitiateUnpause();
        }
        //do countdown
        if (countdown.enabled)
        {
            countdownTimeRemaining -= Time.deltaTime;
            countdown.text = Mathf.RoundToInt(countdownTimeRemaining).ToString();
            if (countdownTimeRemaining <= 0.5f)
            {
                countdown.enabled = false;
                Unpause();
            }
        }
    }

    public void Pause()
    {
        currentGameState = gameState.paused;
        //display pause menu
        restartButton.SetActive(true);
        unpauseButton.SetActive(true);
        pauseButton.SetActive(false);
        pauseText.enabled = true;
        cursorOnButton = false;
        //freeze characters
        charMan.OnFreeze(true);

        //prevents pause sound from playing on start
        if (preventsoundonstartup == false)
        {
            GMaudiosource.PlayOneShot(pausesound);
        }
        else
        {
            preventsoundonstartup = false;
        }
        //stops background
        if (GMbackground.isPlaying)
        {
            GMbackground.Pause();
        }
        
    }

    public void InitiateUnpause()
    {
        //disable pause menu components
        pauseText.enabled = false;
        restartButton.SetActive(false);
        unpauseButton.SetActive(false);
        cursorOnButton = false;
        //initiate countdown
        if (skipCountdown)
            Unpause();
        else
        {
            countdown.enabled = true;
            countdownTimeRemaining = 3.49f; //countdown from 3.5s
            GMaudiosource.PlayOneShot(countdownstatic);
        }
    }

    public void Unpause()
    {
        currentGameState = gameState.running;
        //display pause button
        pauseButton.SetActive(true);
        //unfreeze characters
        charMan.OnFreeze(false);

        GMbackground.Play();
    }

    public void GameOver()
    {
        //display result menu
        currentGameState = gameState.over;
        resultText.enabled = true;
        restartButton.SetActive(true);
        pauseButton.SetActive(false);
        //freeze characters
        charMan.OnFreeze(true);
    }

    //true for tukki, false for champ
    public void IncreaseScore(bool scorer)
    {
        if (scorer)
            tukkiScore++;
        else
            champScore++;
        //win condition
        if (tukkiScore >= scoreLimit)
        {
            resultText.text = "tukki wins!";
            GameOver();
        }
        else if (champScore >= scoreLimit)
        {
            resultText.text = "champ wins!";
            GameOver();
        }
        /*old buggy version of win condition
        if (tukkiScore >= scoreLimit || champScore >= scoreLimit)
        {
            //resultText.text = scorer ? "tukki wins!" : "champ wins!";
            GameOver();                        
        }
        */
        scoreText.text = tukkiScore.ToString() + " (Tukki) - " + champScore.ToString() + " (Champ)";
    }

    public void Restart()
    {
        //hud reset
        tukkiScore = 0;
        champScore = 0;
        scoreText.text = "0 (Tukki) - 0 (Champ)";
        pauseText.enabled = false;
        resultText.enabled = false;
        //character reset
        charMan.Default();
        //prey reset
        preyMan.Restart();
        //unpause
        InitiateUnpause();
    }
}
