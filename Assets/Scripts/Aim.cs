﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Aim : MonoBehaviour {

    //references
    public GameObject cursorTracker;

    public void TargetCursor(bool rotSpeedIsRestricted, float rotSpeedLimit)
    {
        //get the cursor coordinates in world space
        Vector3 cursorPixelPos = Input.mousePosition;
        cursorPixelPos.z = 10f;
        Vector3 cursorWorldPos = Camera.main.ScreenToWorldPoint(cursorPixelPos);
        //create vector from champ to cursor
        Vector3 champToCursor = cursorWorldPos - transform.position;
        //rotate help object to face cursor
        cursorTracker.transform.rotation = Quaternion.FromToRotation(Vector3.up, champToCursor);
        //get rotation angle from help object
        float desiredRotation = cursorTracker.transform.eulerAngles.z;
        //calculate smallest possible rotation that leads from current orientation to facing the cursor
        float desiredTurnAngle = desiredRotation - transform.eulerAngles.z;
        if (desiredTurnAngle > 180f)
            desiredTurnAngle -= 360f;
        else if (desiredTurnAngle < -180f)
            desiredTurnAngle = 360f + desiredTurnAngle;
        float permittedTurnAngle = desiredTurnAngle;
        //restrict rotation to not rotate faster than specified degree/sec limit unless restriction is disabled
        if (rotSpeedIsRestricted)
            permittedTurnAngle = Mathf.Clamp(desiredTurnAngle, -rotSpeedLimit * Time.deltaTime, rotSpeedLimit * Time.deltaTime);
        //rotate aim
        transform.Rotate(new Vector3(0f, 0f, permittedTurnAngle));
    }

}
