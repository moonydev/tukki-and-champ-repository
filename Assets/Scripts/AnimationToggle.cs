﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class AnimationToggle : MonoBehaviour {

    //references
    Animator anim;

    //status info
    bool isFrozen;

	void Start () {
        //reference assignment
        anim = GetComponent<Animator>();
	}

    private void OnEnable()
    {
        CharacterManager.FreezeEvent += ToggleAnim;
    }

    private void OnDisable()
    {
        CharacterManager.FreezeEvent -= ToggleAnim;
    }

    void ToggleAnim (bool desired)
    {
        //only if animator is not already in desired state
        if (isFrozen != desired)
        {
            //toggle animator to freeze/unfreeze animatons
            anim.enabled = !anim.enabled;
            isFrozen = !isFrozen;
        }
    }
}
