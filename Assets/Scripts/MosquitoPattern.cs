﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MosquitoPattern : MonoBehaviour {

    //parameters
    public float speed;
    public float interval;
    public float range;

    //references
    Interface interf;
    Prey prey;

    //status info
    float timeSinceLastMove;
    bool isMoving;
    Vector3 targetPos;
    Vector2 rootPos;
    Vector3 direction;
    Vector3 startPos;
    float startDistance;

    private void Start()
    {
        //reference assignment
        interf = FindObjectOfType<Interface>().GetComponent<Interface>();
        prey = GetComponent<Prey>();
        //store root position
        rootPos = prey.goalPos;
        //vary first interval randomly to avoid synchronized movement with other instances
        timeSinceLastMove += Random.Range(-1f, 1f);
    }

    void Update()
    {
        if (interf.currentGameState == 0 && !prey.isFlyingIn)
        {
            //move
            if (isMoving)
            {
                transform.position += direction * speed * Time.deltaTime;
                if (Vector3.Distance(startPos, transform.position) > startDistance)
                    isMoving = false;
            }
            //set destination
            else if (timeSinceLastMove > interval)
            {
                timeSinceLastMove = 0f;
                startPos = transform.position;
                targetPos = rootPos + Random.insideUnitCircle * range;
                direction = (targetPos - transform.position).normalized;
                startDistance = Vector3.Distance(startPos, targetPos);
                isMoving = true;

            }
            //wait
            else
                timeSinceLastMove += Time.deltaTime;
        }
    }
}
